<h3 class="formtitle"><?php echo lang('edit_user_heading'); ?></h3>

<div id="infoMessage"><?php echo $message; ?></div>
<div class="fieldhold">
    <?php echo form_open(uri_string(), array('id' => 'edit_reg_form', 'onSubmit' => "return validate(this)")); ?>
    <div class="row margin-top-10">
        <div class="col-sm-2">
            <?php echo lang('create_user_surname', 'surname'); ?>
        </div>
        <div class="col-sm-6">
            <?php echo form_input($surname); ?>
            <div class="regErorr1 text-danger"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row margin-top-10">
        <div class="col-sm-2"><?php echo lang('create_user_sa_id', 'sa_id_no'); ?></div>
        <div class="col-sm-6">
            <?php echo form_input($sa_id_no); ?>
            <div class="regErorr2 text-danger"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row margin-top-10">
        <div class="col-sm-2"><?php echo lang('create_user_phone_label', 'mobile_no'); ?></div>
        <div class="col-sm-6">
            <?php echo form_input($mobile_no); ?>
            <div class="regErorr3 text-danger"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row margin-top-10">
        <div class="col-sm-2"><?php echo lang('create_user_email_label', 'email'); ?></div>
        <div class="col-sm-6">
            <?php echo form_input($email); ?>
            <div class="regErorr4 text-danger"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row margin-top-10">
        <div class="col-sm-2"><?php echo lang('create_user_dob', 'bith_date'); ?></div>
        <div class="col-sm-6">
            <?php echo form_input($bith_date); ?>
            <div class="regErorr5 text-danger"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row margin-top-10">
        <div class="col-sm-2"><?php echo lang('create_user_lang', 'language'); ?></div>
        <div class="col-sm-6">
            <select name="language" id="language" data-error=".regErorr6" class="form-control">
                <option value="">Select Language</option>
                <?php
                if (isset($language_list) && !empty($language_list)) {
                    foreach ($language_list as $language) {
                        if (isset($user) && $user['language_id'] == $language['id']) {
                            $selected = 'selected';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="<?php echo $language['id']; ?>" <?php echo isset($selected) ? $selected : ''; ?>><?php echo $language['language']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <div class="regErorr6 text-danger"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row margin-top-10">
        <div class="col-sm-2"><?php echo lang('create_user_iterests', 'interests'); ?></div>
        <div class="col-sm-6">
            <select class="form-control multiselect" name="interests[]" id="interests" data-error=".regErorr7" multiple required>
                <?php
                $interest_arr = explode(',', $user['interest_id']);
                if (isset($interests_list) && !empty($interests_list)) {
                    foreach ($interests_list as $interest) {
                        if (in_array($interest['id'], $interest_arr)) {
                            $selected = 'selected';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="<?php echo $interest['id']; ?>" <?php echo isset($selected) ? $selected : ''; ?>><?php echo $interest['interest']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <div class="lang-cls text-danger"></div>
            <div class="regErorr7 text-danger"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row margin-top-10">
        <div class="col-sm-2"></div>
        <div class="col-sm-6 margin-top-10">
            <div class="input-field col-md-2 pd-0">
                <?php echo form_submit('submit', lang('edit_user_submit_btn'), array('class' => 'btn btn-sm btn-primary')); ?>
            </div>

            <div class="input-field col-md-6 pd-0">
                <a href="<?php echo base_url('/'); ?>"><div class="btn btn-sm btn-secondary">Cancel</div></a>
            </div>

        </div>
    </div>

    <?php echo form_hidden('id', $user['id']); ?>
    <?php echo form_hidden($csrf); ?>
    <?php echo form_close(); ?>
</div>
<script>
    function validate(formObj)
    {
        var selObj = formObj['interests[]'];
        var retVal = false;

        for (var i = 0; i < selObj.length; i++) {
            if (selObj.options[i].selected) {
                retVal = true;
            } else {

            }
        }
        if (retVal == true)
        {
            $('.lang-cls').html('');
        } else
        {
            $('.lang-cls').html('This field is required.');
        }
        return (retVal);
    }
</script>
<style>
    .error{
        color:#a94442;
    }
</style>