<?php
if (isset($users)) {
    foreach ($users as $result) {
        ?>
        <div class="modal fade" id="viewCandidateDetails_<?php echo $result['id'] ?>" role="dialog">
            <div class="modal-dialog modal-md">
                <!-- Modal content-->
                <div class="modal-content">
                    <?php //var_dump($result); ?>
                    <div class="modal-header all-padding-10">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">User  Details</h4>
                        <h4 class="modal-title text-bold "><span ><?php echo $result['first_name'] ?></span></h4>
                    </div>
                    <!-- modal body here -->
                    <div class="user-normal-slim">
                        <div class="modal-body">
                            <div class="candidate-timeline-bg">
                                <div class="row">
                                    <div class="">
                                        <table class="table table-bordered">                      
                                            <tbody>
                                                <tr>
                                                    <td width="40%" class="lft-td">Surname</td>
                                                    <td><?php echo $result['surname']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="lft-td">South African ID No.</td>
                                                    <td><?php echo $result['sa_id_number']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="lft-td">Mobile No.</td>
                                                    <td><?php echo $result['phone']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="lft-td">Email</td>
                                                    <td><?php echo $result['email']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="lft-td">DOB</td>
                                                    <td><?php echo date("j F, Y", strtotime($result['birth_date'])); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="lft-td">Language</td>
                                                    <td><?php echo $result['language']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="lft-td">Interests</td>
                                                    <td><?php echo isset($result['interests']) ? implode(',', $result['interests']) : ''; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="lft-td">Status</td>
                                                    <td><?php echo ($result['active'] == 1) ? 'Active' : 'In-Active'; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>