<?php // echo $this->session->flashdata('success_msg');                   ?>
<div class="row-fluid">
    <div class="pull-left" id="page-header"><h3 class=""><?php echo lang('index_heading'); ?></h3></div>
    <div class="pull-right" id="add-link"><a href="<?php echo base_url() . 'auth/create_user' ?>" id="create_user"><button class="btn btn-default"><i class="fa fa-plus-circle"></i> Create User</button></a></div>
</div>

<!--<div id="infoMessage"><?php echo $message; ?></div>-->
<div class="user-container row">
    <table id="FlagsExport" class="table table-bordered table-striped dt-responsive display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Surname</th>
                <th>South African ID No.</th>
                <th>Mobile No.</th>
                <th>Email</th>
                <th>DOB</th>
                <th>Language</th>
                <th>Interests</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
                <tr id="deleteRow_<?php echo $user['id'] ?>">
                    <td class="surname"><a data-toggle="modal" href="#viewCandidateDetails_<?php echo $user['id'] ?>"><?php echo $user['surname']; ?></td>
                    <td><?php echo htmlspecialchars($user['sa_id_number'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td><?php echo htmlspecialchars($user['phone'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td><?php echo htmlspecialchars($user['email'], ENT_QUOTES, 'UTF-8'); ?></td>
                    <td><?php echo date("j F, Y", strtotime($user['birth_date'])); ?></td>
                    <td><?php echo $user['language'] ?></td>
                    <td><?php echo isset($user['interests']) ? implode(',', $user['interests']) : ''; ?></td>
                    <td><?php echo ($user['active'] == 1) ? 'Active' : 'In-Active'; ?></td>
                    <td>
                        <?php if ($user['active'] == 1) { ?>
                            <?php echo anchor("auth/edit_user/" . $user['id'], '<i class="fa fa-pencil" aria-hidden="true" title="Edit"></i>'); ?>&nbsp;&nbsp;<i class="fa fa-trash text-danger" aria-hidden="true" title="Delete" data-toggle="modal" data-target="#confirm-delete" data-userid="<?php echo $user['id']; ?>"></i>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#FlagsExport').DataTable({
            "pageLength": 50,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
        });
    });
<?php // if (isset($message)) {
?>
//        showSuccess('<?php echo $message; ?>');
<?php // }
?>
</script>

<!--<link type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" rel="stylesheet">-->
<link type="text/css" href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" rel="stylesheet">
<!--<script type="text/javascript" src="jquery-1.12.0.min.js"></script>-->

<!--<script type="text/javascript" src="https://cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>-->
<script type="text/javascript" src="https://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>


<?php $this->load->view('modal/show_user') ?>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirmation </h4>
            </div>
            <div class="modal-body status-class">      
                <p>Are you sure you want to delete this record?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning2 btn-sm btn-ok">Yes</button>
                <button type="button" class="btn btn-default btn-sm redbg" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<script>
    function deleteUser(dId) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>auth/delete_user',
            data: {'delete_id': dId},
            success: function (data) {
                $('#deleteRow_' + dId).remove();
                $('#confirm-delete').modal('hide');
                $('.modal-backdrop').css('display', 'none');
                $("#infoMessage").html('Record deleted successfully');
                $("#success_msg").html('<a href="#" class="close" data-dismiss="alert">&times;</a><strong>Success!</strong>Record deleted successfully');
                $("#success_msg").css("display", "block");
                setTimeout(function () {
                    $('#success_msg').fadeOut('fast');
                }, 3000);
                //                showSuccess("Record deleted successfully");
                var count = $('#FlagsExport tr').length;
                if (count <= 1) {
                    $('#FlagsExport tbody').html('<tr><td colspan="9">No Data Available</td></tr>');
                }
            }
        });
        return false;
    }
</script>
<script>
    $(document).ready(function () {
//        showSuccess("Record deleted successfully");
        $('#confirm-delete').on('click', '.btn-ok', function (e) {
            var $modalDiv = $(e.delegateTarget);
            var id = $(this).data('user_id');
            deleteUser(id);
        });
        $('#confirm-delete').on('show.bs.modal', function (e) {
            var data = $(e.relatedTarget).data();
            $('.btn-ok', this).data('user_id', data.userid);
        });
    });
</script> 