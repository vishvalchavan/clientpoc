<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Interests extends MY_Model {

    //put your code here
    public $_table = 'interests';
    public $primary_key = 'id';

    public function get_allInterests() {
        $this->db->select('id,interest');
        $this->db->from('interests');
        $this->db->where(array('isactive' => 1, 'isdeleted' => 0));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_interest_by_ids($ids) {
        $this->db->select('id,interest');
        $this->db->from('interests');
        $this->db->where_in('id', $ids);
        $this->db->where(array('isactive' => 1, 'isdeleted' => 0));
        $query = $this->db->get()->result_array();
        $objdata = array();
        if (isset($query)) {
            foreach ($query as $val) {
                $objdata[$val['id']] = $val['interest'];
            }
        }
        return $objdata;
    }

}
