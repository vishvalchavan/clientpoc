/*Login */

$("#login_form").validate({
    rules: {
        identity: {
            required: true,
            minlength: 2,
            maxlength: 25,

        },
        password: {
            required: true,
            minlength: 4,
            maxlength: 12,

        },

        req_no_positions: {
            required: true,

        },
    },
    //For custom messages
    messages: {
        uname: {
            required: "Enter a username",
            minlength: "Enter at least 5 characters"
        },
        curl: "Enter your website",
    },
    errorElement: 'div',
    errorPlacement: function (error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});
/*End Login */


/*Registration */

$("#reg_form").validate({
    rules: {
        surname: {
            required: true,
            minlength: 2,
            maxlength: 25,
            lettersonly: true,
//            noSpace: true,
        },
        sa_id_no: {
            number: true,
            required: true,
            minlength: 13,
            maxlength: 13,
//            noSpace: true,

        },
        mobile_no: {
            number: true,
            required: true,
//            phoneUS: true,
            minlength: 10,
            maxlength: 12,
//            noSpace: true,

        },
        email: {
            required: true,
            email: true,
//            noSpace: true,

        },
        bith_date: {
            required: true,
            date: true
        },
        language: {
            required: true,
        },
        interests: {
            required: true,
        },

    },
    //For custom messages
    messages: {
        sa_id_no: {
            minlength: "Enter at least 13 digits",
            maxlength: "Maximum 13 digits are allowed"
        },
        mobile_no: {
            minlength: "Enter at least 10 digits",
            maxlength: "Maximum 12 digits are allowed"
        },
        curl: "Enter your website",
    },
    errorElement: 'div',
    errorPlacement: function (error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});
$("#edit_reg_form").validate({
    rules: {
        surname: {
            required: true,
            lettersonly: true,
            minlength: 2,
            maxlength: 25,
//            noSpace: true,
        },
        sa_id_no: {
            number: true,
            required: true,
            minlength: 13,
            maxlength: 13,
//            noSpace: true,

        },
        mobile_no: {
            number: true,
            required: true,
//            phoneUS: true,
            minlength: 10,
            maxlength: 12,
//            noSpace: true,
        },
        email: {
            required: true,
            email: true,
//            noSpace: true,
        },
        bith_date: {
            required: true,
            date: true
        },
        language: {
            required: true,
        },
        interests: {
            required: true,
        },

    },
    //For custom messages
    messages: {
        sa_id_no: {
            minlength: "Enter at least 13 digits",
            maxlength: "Maximum 13 digits are allowed"
        },
        mobile_no: {
            minlength: "Enter at least 10 digits",
            maxlength: "Maximum 12 digits are allowed"
        },
        curl: "Enter your website",
    },
    errorElement: 'div',
    errorPlacement: function (error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});
/*End Registration */